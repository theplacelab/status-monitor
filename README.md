# Status Monitor

## Docker

Security Note: this runs in privlieged mode to access the USB device (blink1), which is fine if you are running it locally but probably not the best idea in a shared environment.

```sh
docker run -d \
--restart unless-stopped \
-p:7777:8888 \
--privileged -v /dev/bus/usb:/dev/bus/usb \
--mount type=bind,\
source=*PATH TO CONFIG*,target=/usr/src/app/config.json \
registry.gitlab.com/theplacelab/status-monitor
```

## Manual

### Setup Systemd (Optional)

Modify any settings or paths in the ./deploy/systemd directory

Move these files to /etc/systemd/system

sudo systemctl start fr-statusmonitor.service

### Remap port 8888 to 80 (Optional)

You will note the frontend is configured to listen on port 8888. If you prefer port 80, you will need to remap that since only root can run on those ports. The easiest thing to do is set up an iptable rule:
iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8888

iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 8888

iptables-save -f /etc/iptables/iptables.rules

```

```
