const Blink1 = require("node-blink1");
const fetch = require("node-fetch");
const dns = require("dns");
const fs = require("fs");
const connect = require("connect");
const express = require("express");
const app = express();
const config = require("./config.json");
const nodeCleanup = require("node-cleanup");

// Sort alpha by displayname
config.sitelist = config.sitelist.sort((a, b) =>
  a.displayName > b.displayName ? 1 : -1
);

// Setup web server
app.listen(config.port, () => {
  console.log(`SERVER ON: ${config.port}`);
});

app.use(express.static(`${__dirname}/www`, { etag: false, maxAge: 0 }));

app.get("/docheck", (req, res) => {
  doCheck();
  res.send(200, { message: "Checking..." });
});

// Check to see if blink(1) connected
const hasBlink = Blink1.devices().length > 0;
let blink1;
if (hasBlink) {
  blink1 = new Blink1();
} else {
  console.warn("WARNING: Missing blink(1)");
}

const setColor = (color) => {
  if (hasBlink) {
    if (color.blink) {
      blink1.writePatternLine(200, color.r, color.g, color.b, 0);
      blink1.writePatternLine(200, 0, 0, 0, 1);
      blink1.playLoop(0, 1, 0);
    } else {
      blink1.fadeToRGB(250, color.r, color.g, color.b);
    }
  }
};
setColor(config.color.off);

const rebuildStatusPage = (issues) => {
  const now = new Date();
  content = `<h1>${issues.length === 0 ? "😀" : "😳"} ${now.toLocaleString(
    "en-US"
  )}</h1>`;
  if (issues.length > 0) {
    content +=
      "<table id='issues'><tr><th>Site</th><th>Expected</th><th>Got</th></tr>";
    issues.forEach((issue) => {
      content += `<tr><td><a href="${issue.url}">${issue.displayName}</a></td><td>${issue.expect}</td><td>${issue.got}</td></tr>`;
    });
    content += "</table>";
  } else {
    content += "<div id='no-issues'>All Good!</div>";
  }

  content += "<h2>Rules</h2>";
  content += "<table id='rules'><tr><th>Site</th><th>Expected</th></tr>";
  config.sitelist.forEach((site) => {
    content += `<tr><td>${site.disabled ? "⚪️" : "🟢"}<a href="${site.url}">${
      site.displayName
    }</a></td><td>${site.expect}</td></tr>`;
  });
  content += "</table>";
  fs.readFile(`${__dirname}/www/template.html`, "utf8", (err, templateData) => {
    fs.writeFile(
      `${__dirname}/www/index.html`,
      templateData.replace("~~CONTENT~~", content),
      "utf8",
      () => {}
    );
  });
};

const doCheck = () => {
  setColor(config.color.working);

  // Check for connectivity
  dns.lookupService("8.8.8.8", 53, (err) => {
    if (err) {
      rebuildStatusPage([
        { displayName: "", expect: "", got: "*** NETWORK UNREACHABLE ***" }
      ]);
      setColor(config.color.offline);
    } else {
      var promises = config.sitelist.map((site) => {
        if (site.disabled) {
          return {
            status: "SKIPPED"
          };
        } else {
          return fetch(site.url)
            .then((res) => res)
            .catch(() => {
              return {
                status: "UNREACHABLE"
              };
            });
        }
      });

      let issues = [];
      Promise.all(promises).then((results) => {
        statusColor = config.color.ok;
        results.forEach((result, idx) => {
          if (
            config.sitelist[idx].expect !== result.status &&
            !config.sitelist[idx].disabled
          ) {
            issues.push({
              ...config.sitelist[idx],
              got: result.status
            });
            statusColor = config.color.error;
          }
        });
        rebuildStatusPage(issues);
        setColor(statusColor);
      });
    }
  });
};

console.log(`CHECKING: Now and every ${config.intervalInMinutes} minutes`);
doCheck();
setInterval(() => {
  doCheck();
}, (config.intervalInMinutes || 1) * 60 * 1000);

nodeCleanup(function (exitCode, signal) {
  setColor(config.color.off);
});
